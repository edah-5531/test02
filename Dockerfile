FROM azul/zulu-openjdk-centos:17
COPY ./src /usr/src
WORKDIR /usr/src/
RUN javac App.java
CMD ["java", "App"]